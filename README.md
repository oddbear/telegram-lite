# Telegram-Lite

_TelegramLite class inherits all `node-telegram-bot-api` methods_

The `telegram-lite` module provides an easier way to use Telegram than the native API. It offers several convenient features:

- The `callbackQuery` now accepts a callback function instead of `callback_data`.
- When a message, callback query, or other event is caught in the onMessage listener, it provides additional methods such as `answer`, `reply`, `copy`, and `forward`.
- It includes a private chats method called `input` that waits for user input, which can be easily canceled by `chatId`.
- Command handlers for messages automatically handle specified commands.

❗️ For now each session will lose previous context of `input` and `callbackQuery`, in future it should be wrapper functions that do all the job.

## Installation

```bash
npm install -S telegram-lite
```

## Usage Example

```typescript
import TelegramLite from 'telegram-lite';

const telegramLite = new TelegramLite('<YOUR_TOKEN>');

telegramLite.addMessageCommands([
  {
    text: '/start',
    callback: (context, message) => {
      // Send an answer in the same chat
      message.answer({
        text: 'Welcome to Telegram Lite bot!',
        reply_markup: {
          // Provide keyboard with callback function
          inline_keyboard: [
            [
              {
                text: 'Click me!',
                callback: (context, query) => {
                  // Send a message to the same chat
                  message.reply({ text: 'You clicked me!' });
                }
              }
            ]
          ]
        }
      });

      // Reply to the handled message
      message.reply({ text: 'Your message was successfully handled' });

      // Forward the handled message to a specific chat
      message.forward({ chat_id: -1111111 });

      // Copy the handled message to a specific chat
      message.copy({ chat_id: -1111111 });
    }  
  }
]);
```

## To wait for input
```typescript
telegramLite.input({
   text: "Hi! I'm waiting for your response"
}, {
  callback: (context, message) => {
    // Return true to stop waiting for the user's response
    // or false to continue waiting
    if (message.text === 'hi') {
      return true;
    }
    return false;
  }
});
```

## Middlewares
You can provide your own middlewares, and their results will be sent to the context in all your callbacks.

```typescript
const telegramLite = new TelegramLite('<YOUR_TOKEN>', {
  middlewares: [
    async (context, data) => {
      return {
        ...context,
        user: await getUser(context, data.from.id),
      };
    }
  ]
});

telegramLite.addCommandListener({
  text: '/start',
  callback: (context, message) => {
    message.reply({
      text: `Hi, ${context.user.name}!`,
    });
  }
});
```

## License
This module is licensed under the [MIT License](https://opensource.org/licenses/MIT).
