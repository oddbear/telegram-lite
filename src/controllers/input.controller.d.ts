import { TelegramContext } from '../types';
import { InputCommand } from '../types/inputCommand.type';
import { Methods } from '../types/constructor.type';
import TelegramBot from 'node-telegram-bot-api';
export declare class InputController<Context extends TelegramContext> {
    private readonly methods;
    private chatIdToInputOptionsMap;
    constructor(methods: Methods<Context>);
    addInputCommand(inputCommand: InputCommand<Context>): () => void;
    cancelInput(chatId: TelegramBot.ChatId): void;
    getInputCommand(chatId: TelegramBot.ChatId): InputCommand<Context> | undefined;
}
