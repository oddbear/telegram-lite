"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConstructorController = void 0;
var node_telegram_bot_api_1 = require("node-telegram-bot-api");
var message_controller_1 = require("./message.controller");
var callbackQuery_controller_1 = require("./callbackQuery.controller");
var data_1 = require("../utils/data");
var string_1 = require("../utils/string");
var telegram_1 = require("../utils/telegram");
var message_entity_1 = require("../entities/message.entity");
var constants_1 = require("../utils/constants");
var callback_service_1 = require("../services/callback.service");
var input_controller_1 = require("./input.controller");
var chat_controller_1 = require("./chat.controller");
var ConstructorController = /** @class */ (function () {
    function ConstructorController(botToken, options) {
        var _this = this;
        var _a, _b, _c;
        this.botToken = botToken;
        this.options = options;
        this.botUsername = undefined;
        this.listeners = new Map();
        this.callbackController = new callback_service_1.CallbackService();
        this.callbackQueryController = new callbackQuery_controller_1.CallbackQueryController(this.getMethods(), this.callbackController);
        this.inputController = new input_controller_1.InputController(this.getMethods());
        this.messageController = new message_controller_1.MessageController(this.getMethods(), this.inputController);
        if (((_c = (_b = (_a = this.options) === null || _a === void 0 ? void 0 : _a.middlewares) === null || _b === void 0 ? void 0 : _b.length) !== null && _c !== void 0 ? _c : -1) === 0) {
            throw new Error('TelegramConstructor create error: middlewares should be array with length > 0 or not specified');
        }
        this.bot = new node_telegram_bot_api_1.default(botToken, {
            polling: true,
        });
        this.chatController = new chat_controller_1.ChatController(this.bot);
        void (0, telegram_1.getBotUsername)(botToken)
            .then(function (botUsername) {
            if (botUsername == null) {
                throw new Error('TelegramConstructor create error: telegramBot username is null');
            }
            _this.botUsername = botUsername;
            _this.addTelegramListenersOnCommands();
            _this.triggerEventListener('ready');
        })
            .catch(function (error) {
            throw new Error("TelegramConstructor create error: ".concat(error.message));
        });
    }
    ConstructorController.prototype.on = function (eventName, callback) {
        var _this = this;
        var _a;
        var events = (_a = this.listeners.get(eventName)) !== null && _a !== void 0 ? _a : [];
        events.push(callback);
        this.listeners.set(eventName, events);
        return function () {
            var _a;
            var events = (_a = _this.listeners.get(eventName)) !== null && _a !== void 0 ? _a : [];
            var index = events.indexOf(callback);
            if (index !== -1) {
                events.splice(index, 1);
            }
        };
    };
    ConstructorController.prototype.addMessageCommand = function (command) {
        var messageController = this.messageController;
        if (messageController == null) {
            throw new Error('addMessageCommands error: messageController is not loaded yet, probably it is the problem in module');
        }
        return messageController.addCommand(command);
    };
    ConstructorController.prototype.addMessageCommands = function (messageCommands) {
        var commands = [];
        for (var _i = 0, messageCommands_1 = messageCommands; _i < messageCommands_1.length; _i++) {
            var messageCommand = messageCommands_1[_i];
            commands.push(this.addMessageCommand(messageCommand));
        }
        return commands;
    };
    ConstructorController.prototype.input = function (sendOptions, inputOptions) {
        return __awaiter(this, void 0, void 0, function () {
            var chatId, chat;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        chatId = sendOptions.chat_id;
                        if (chatId == null) {
                            throw new Error('input error: chat_id should be specified');
                        }
                        return [4 /*yield*/, this.chatController.getChat(chatId)];
                    case 1:
                        chat = _a.sent();
                        if (chat.type !== 'private') {
                            throw new Error('input error: input could not be used not in private chat');
                        }
                        this.inputController.addInputCommand({
                            callback: inputOptions.callback,
                            chat_id: sendOptions.chat_id,
                        });
                        return [2 /*return*/, this.send(sendOptions)];
                }
            });
        });
    };
    ConstructorController.prototype.cancelInput = function (chatId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.inputController.cancelInput(chatId)];
            });
        });
    };
    ConstructorController.prototype.edit = function (editOptions) {
        return __awaiter(this, void 0, void 0, function () {
            var bot, replyMarkup, options, nerd, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        bot = this.bot;
                        replyMarkup = this.getUpdatedReplyMarkup(editOptions, true);
                        options = __assign(__assign({}, editOptions), { reply_markup: replyMarkup });
                        nerd = function (callback) {
                            var args = [];
                            for (var _i = 1; _i < arguments.length; _i++) {
                                args[_i - 1] = arguments[_i];
                            }
                            return (0, data_1.nerdFunction)(options, callback.bind(bot)).apply(void 0, args);
                        };
                        if (!('text' in options)) return [3 /*break*/, 2];
                        return [4 /*yield*/, nerd(bot.editMessageText, options.text, options)];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 4];
                    case 2:
                        if (!['photo', 'video', 'animation', 'audio', 'voice'].some(function (key) { return key in options; })) return [3 /*break*/, 4];
                        return [4 /*yield*/, nerd(bot.editMessageMedia, options, options)];
                    case 3:
                        result = _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ConstructorController.prototype.send = function (sendOptions) {
        return __awaiter(this, void 0, void 0, function () {
            var bot, replyMarkup, options, nerd, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        bot = this.bot;
                        replyMarkup = this.getUpdatedReplyMarkup(sendOptions);
                        options = __assign(__assign({}, sendOptions), { reply_markup: replyMarkup });
                        nerd = function (callback) {
                            var args = [];
                            for (var _i = 1; _i < arguments.length; _i++) {
                                args[_i - 1] = arguments[_i];
                            }
                            return (0, data_1.nerdFunction)(options, callback.bind(bot)).apply(void 0, args);
                        };
                        if (!('text' in options)) return [3 /*break*/, 2];
                        return [4 /*yield*/, nerd(bot.sendMessage, options.chat_id, options.text, options)];
                    case 1:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 2:
                        if (!('photo' in options)) return [3 /*break*/, 4];
                        return [4 /*yield*/, nerd(bot.sendPhoto, options.chat_id, options.photo, options, options)];
                    case 3:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 4:
                        if (!('media' in options)) return [3 /*break*/, 6];
                        return [4 /*yield*/, nerd(bot.sendMediaGroup, options.chat_id, options.media, options)];
                    case 5:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 6:
                        if (!('video' in options)) return [3 /*break*/, 8];
                        return [4 /*yield*/, nerd(bot.sendVideo, options.chat_id, options.video, options, options)];
                    case 7:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 8:
                        if (!('video_note' in options)) return [3 /*break*/, 10];
                        return [4 /*yield*/, nerd(bot.sendVideoNote, options.chat_id, options.video_note, options, options)];
                    case 9:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 10:
                        if (!('animation' in options)) return [3 /*break*/, 12];
                        return [4 /*yield*/, nerd(bot.sendAnimation, options.chat_id, options.animation, options)];
                    case 11:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 12:
                        if (!('audio' in options)) return [3 /*break*/, 14];
                        return [4 /*yield*/, nerd(bot.sendAudio, options.chat_id, options.audio, options, options)];
                    case 13:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 14:
                        if (!('voice' in options)) return [3 /*break*/, 16];
                        return [4 /*yield*/, nerd(bot.sendVoice, options.chat_id, options.voice, options, options)];
                    case 15:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 16:
                        if (!('question' in options)) return [3 /*break*/, 18];
                        return [4 /*yield*/, nerd(bot.sendPoll, options.chat_id, options.question, options.poll_options, options)];
                    case 17:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 18:
                        if (!('sticker' in options)) return [3 /*break*/, 20];
                        return [4 /*yield*/, nerd(bot.sendSticker, options.chat_id, options.sticker, options, options)];
                    case 19:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 20:
                        if (!('document' in options)) return [3 /*break*/, 22];
                        return [4 /*yield*/, nerd(bot.sendDocument, options.chat_id, options.document, options, options)];
                    case 21:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 22:
                        if (!('emoji' in options)) return [3 /*break*/, 24];
                        return [4 /*yield*/, nerd(bot.sendDice, options.chat_id, options)];
                    case 23:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 24:
                        if (!('game_short_name' in options)) return [3 /*break*/, 26];
                        return [4 /*yield*/, nerd(bot.sendGame, options.chat_id, options.game_short_name, options)];
                    case 25:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 26:
                        if (!('provider_token' in options)) return [3 /*break*/, 28];
                        return [4 /*yield*/, nerd(bot.sendInvoice, options.chat_id, options.title, options.description, options.payload, options.provider_token, options.currency, options.prices, options)];
                    case 27:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 28:
                        if (!('address' in options)) return [3 /*break*/, 30];
                        return [4 /*yield*/, nerd(bot.sendVenue, options.chat_id, options.latitude, options.longitude, options.title, options.address, options)];
                    case 29:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 30:
                        if (!('latitude' in options)) return [3 /*break*/, 32];
                        return [4 /*yield*/, nerd(bot.sendLocation, options.chat_id, options.latitude, options.longitude, options)];
                    case 31:
                        result = _a.sent();
                        return [3 /*break*/, 34];
                    case 32:
                        if (!('phone_number' in options)) return [3 /*break*/, 34];
                        return [4 /*yield*/, nerd(bot.sendContact, options.chat_id, options.phone_number, options.first_name, options)];
                    case 33:
                        result = _a.sent();
                        _a.label = 34;
                    case 34:
                        if (result == null) {
                            throw new Error("send error: invalid options sent\n".concat((0, string_1.stringify)(options)));
                        }
                        return [2 /*return*/, new message_entity_1.MessageEntity(this.getMethods(), result)];
                }
            });
        });
    };
    ConstructorController.prototype.forward = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var bot, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        bot = this.bot;
                        return [4 /*yield*/, (0, data_1.nerdFunction)(options, bot.forwardMessage.bind(bot))(options.chat_id, options.from_chat_id, options.message_id, options)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, new message_entity_1.MessageEntity(this.getMethods(), result)];
                }
            });
        });
    };
    ConstructorController.prototype.copy = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var bot, replyMarkup;
            return __generator(this, function (_a) {
                bot = this.bot;
                replyMarkup = this.getUpdatedReplyMarkup(options);
                return [2 /*return*/, (0, data_1.nerdFunction)(options, bot.copyMessage.bind(bot))(options.chat_id, options.from_chat_id, options.message_id, __assign(__assign({}, options), { reply_markup: replyMarkup }))];
            });
        });
    };
    ConstructorController.prototype.getUpdatedReplyMarkup = function (options, onlyInline) {
        var _a;
        var _b = this, callbackController = _b.callbackController, constructorOptions = _b.options;
        var maxButtonsInRow = (_a = constructorOptions === null || constructorOptions === void 0 ? void 0 : constructorOptions.max_buttons_in_row) !== null && _a !== void 0 ? _a : constants_1.DEFAULT_MAX_BUTTONS_IN_ROW;
        if ('reply_markup' in options && options.reply_markup != null) {
            if ('inline_keyboard' in options.reply_markup && options.reply_markup.inline_keyboard != null) {
                return {
                    inline_keyboard: callbackController.prepareInlineKeyboard(options.reply_markup.inline_keyboard, maxButtonsInRow),
                };
            }
            return onlyInline
                ? undefined
                : options.reply_markup;
        }
    };
    ConstructorController.prototype.addTelegramListenersOnCommands = function () {
        var _this = this;
        var bot = this.bot;
        bot.on('polling_error', this.onError.bind(this, 'polling_error'));
        bot.on('webhook_error', this.onError.bind(this, 'webhook_error'));
        bot.on('error', this.onError.bind(this, 'error'));
        bot.on('message', function (message) {
            return _this.messageController.onMessage(_this.getTelegramContext('message', message), message);
        });
        bot.on('callback_query', function (query) {
            return _this.callbackQueryController.onCallback(_this.getTelegramContext('callback_query', query), query);
        });
        // bot.onText(/\/start(?: (.+))?/, bindTelegramBot(onTelegramStartText));
        // telegramBot.on('inline_query', bindTelegramBot(onTelegramInlineQuery));
    };
    ConstructorController.prototype.onError = function (errorName, err) {
        var message = err.message;
        this.triggerEventListener(errorName, err);
        return message;
    };
    ConstructorController.prototype.getTelegramContext = function (eventName, data) {
        var _a, _b;
        if (this.botUsername == null) {
            throw new Error('getTelegramContext error: botUsername is not loaded yet, probably it is the problem in module');
        }
        var telegramContext = {
            bot: this.bot,
            botUsername: this.botUsername,
            botToken: this.botToken,
        };
        // @ts-ignore
        return ((_b = (_a = this.options) === null || _a === void 0 ? void 0 : _a.middlewares) !== null && _b !== void 0 ? _b : []).reduce(function (acc, callback) {
            var result = callback(__assign({}, telegramContext), data);
            return Object.assign(acc, result);
        }, telegramContext);
    };
    ConstructorController.prototype.triggerEventListener = function (eventName, err) {
        var _a;
        var events = (_a = this.listeners.get(eventName)) !== null && _a !== void 0 ? _a : [];
        if (err != null) {
            events.forEach(function (callback) { return callback(err); });
            return;
        }
        events.forEach(function (callback) { return callback(); });
    };
    ConstructorController.prototype.getCallbackByHash = function (hash) {
        return this.callbackController.getCallbackByHash(hash);
    };
    ConstructorController.prototype.getMethods = function () {
        return {
            send: this.send.bind(this),
            forward: this.forward.bind(this),
            copy: this.copy.bind(this),
            input: this.input.bind(this),
            getCallbackByHash: this.getCallbackByHash.bind(this),
        };
    };
    return ConstructorController;
}());
exports.ConstructorController = ConstructorController;
