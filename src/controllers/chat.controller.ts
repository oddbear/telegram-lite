import TelegramBot from 'node-telegram-bot-api';

export class ChatController {
  private chatIdToChatMap = new Map<TelegramBot.ChatId, TelegramBot.Chat>();

  constructor(private readonly getChatMethod: TelegramBot['getChat']) {}

  public async getChat(chatId: TelegramBot.ChatId): Promise<TelegramBot.Chat> {
    const chat = this.chatIdToChatMap.get(chatId);
    if (chat == null) {
      return this.findAndAddChat(chatId);
    }

    return chat;
  }

  private async findAndAddChat(chatId: TelegramBot.ChatId): Promise<TelegramBot.Chat> {
    const chat = await this.getChatMethod(chatId);

    this.chatIdToChatMap.set(chat.id, chat);

    return chat;
  }
}
