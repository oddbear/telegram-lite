import { MessageCommand } from '../types/messageCommand.type';
import TelegramBot from 'node-telegram-bot-api';
import { TelegramContext } from '../types';
import { MessageEntity } from '../entities/message.entity';
import { Methods } from '../types/constructor.type';
import { InputController } from './input.controller';

export class MessageController<Context extends TelegramContext> {
  private readonly commands: MessageCommand<Context>[] = [];

  constructor(private readonly methods: Methods<Context>, private readonly inputController: InputController<Context>) {}

  public addCommand(command: MessageCommand<Context>) {
    this.commands.push(command);

    return () => {
      const index = this.commands.indexOf(command);
      if (index !== -1) {
        this.commands.splice(index, 1);
      }
    };
  }

  public async onMessage(context: Context, message: TelegramBot.Message) {
    const { commands, inputController } = this;
    const {
      text,
      chat: { id: chatId },
    } = message;

    const messageEntity = new MessageEntity(this.methods, message);

    const inputCommand = inputController.getInputCommand(chatId);
    if (inputCommand != null) {
      const result = await inputCommand.callback(context, messageEntity.message);

      if (result) {
        inputController.cancelInput(chatId);
      }

      return;
    }

    const matchedCommand = commands.find((command) => {
      if ('regexp' in command) {
        if (text == null) {
          return false;
        }

        return command.regexp.test(text);
      }

      if ('text' in command) {
        if (text == null) {
          return false;
        }

        return command.text === text.trim();
      }

      if ('validation' in command) {
        return command.validation(context, messageEntity.message);
      }
    });

    if (matchedCommand == null) {
      return;
    }

    matchedCommand.callback(context, messageEntity.message);
  }
}
