import { TelegramContext } from '../types';
import TelegramBot from 'node-telegram-bot-api';
import { CallbackQueryEntity } from '../entities/callbackQuery.entity';
import { Methods } from '../types/constructor.type';
import { CallbackService } from '../services/callback.service';

export class CallbackQueryController<Context extends TelegramContext> {
  constructor(
    private readonly methods: Methods<Context>,
    private readonly callbackController: CallbackService<Context>,
  ) {}

  public onCallback(context: Context, query: TelegramBot.CallbackQuery) {
    const { data } = query;
    if (data == null) {
      return;
    }

    const callback = this.callbackController.getCallbackByHash(data);
    if (callback == null) {
      throw new Error(
        'onCallback error: undefined callbackQuery is used, probably it is the old callbackQuery instance, new inline_buttons should work correctly or you used own callback_data',
      );
    }

    callback(context, new CallbackQueryEntity<Context>(this.methods, query));
  }
}
