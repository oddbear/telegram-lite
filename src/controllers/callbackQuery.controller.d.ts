import { TelegramContext } from '../types';
import TelegramBot from 'node-telegram-bot-api';
import { Methods } from '../types/constructor.type';
import { CallbackService } from '../services/callback.service';
export declare class CallbackQueryController<Context extends TelegramContext> {
    private readonly methods;
    private readonly callbackController;
    constructor(methods: Methods<Context>, callbackController: CallbackService<Context>);
    onCallback(context: Context, query: TelegramBot.CallbackQuery): void;
}
