"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InputController = void 0;
var InputController = /** @class */ (function () {
    function InputController(methods) {
        this.methods = methods;
        this.chatIdToInputOptionsMap = new Map();
    }
    InputController.prototype.addInputCommand = function (inputCommand) {
        var _this = this;
        this.chatIdToInputOptionsMap.set(inputCommand.chat_id, inputCommand);
        return function () {
            _this.chatIdToInputOptionsMap.delete(inputCommand.chat_id);
        };
    };
    InputController.prototype.cancelInput = function (chatId) {
        this.chatIdToInputOptionsMap.delete(chatId);
    };
    InputController.prototype.getInputCommand = function (chatId) {
        var chatIdToInputOptionsMap = this.chatIdToInputOptionsMap;
        return chatIdToInputOptionsMap.get(chatId);
    };
    return InputController;
}());
exports.InputController = InputController;
