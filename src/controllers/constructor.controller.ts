import {
  EventName,
  EventListener,
  TelegramContext,
  TelegramData,
  EventErrorName,
  EventErrorListener,
  TelegramDataEvents,
  SendOptions,
  ForwardOptions,
  CopyOptions,
  MessageCommand,
  EditOptions,
  InputOptions,
} from '../types';
import TelegramBot from 'node-telegram-bot-api';
import { MergeReturnType } from '../types/helper.type';
import { MessageController } from './message.controller';
import { CallbackQueryController } from './callbackQuery.controller';
import { nerdFunction, NerdFunction } from '../utils/data';
import { stringify } from '../utils/string';
import { getBotUsername } from '../utils/telegram';
import { Methods } from '../types/constructor.type';
import { MessageEntity } from '../entities/message.entity';
import { DEFAULT_MAX_BUTTONS_IN_ROW } from '../utils/constants';
import { CallbackService } from '../services/callback.service';
import { InputController } from './input.controller';
import { ChatController } from './chat.controller';

export class ConstructorController<
  Initial extends TelegramContext = TelegramContext,
  Middlewares extends ((arg: TelegramContext, data: TelegramData) => Initial)[] = ((
    arg: TelegramContext,
    data: TelegramData,
  ) => Initial)[],
  Context extends MergeReturnType<Initial, Middlewares> & TelegramContext = MergeReturnType<Initial, Middlewares> &
    TelegramContext,
> extends TelegramBot {
  public botUsername: string | undefined = undefined;

  private readonly chatController: ChatController;

  private readonly listenersMap: Map<EventName | EventErrorName, (EventListener | EventErrorListener)[]> = new Map<
    EventName | EventErrorName,
    (EventListener | EventErrorListener)[]
  >();
  private readonly callbackController = new CallbackService<Context>();
  private readonly callbackQueryController = new CallbackQueryController<Context>(
    this.getMethods(),
    this.callbackController,
  );
  private readonly inputController = new InputController<Context>(this.getMethods());
  private readonly messageController = new MessageController<Context>(this.getMethods(), this.inputController);

  constructor(
    public readonly botToken: string,
    private readonly properties?: {
      middlewares?: Middlewares;
      attempts_count?: number;
      attempts_retry_timeout?: number;
      max_buttons_in_row?: number;
      constructor_options?: TelegramBot.ConstructorOptions;
    },
  ) {
    super(botToken, properties?.constructor_options);

    if ((this.properties?.middlewares?.length ?? -1) === 0) {
      throw new Error('TelegramConstructor create error: middlewares should be array with length > 0 or not specified');
    }

    this.chatController = new ChatController(this.bindSuper('getChat'));

    void getBotUsername(botToken)
      .then((botUsername) => {
        if (botUsername == null) {
          throw new Error('TelegramConstructor create error: telegramBot username is null');
        }
        this.botUsername = botUsername;

        this.addTelegramListenersOnCommands();

        this.triggerEventListener('ready');
      })
      .catch((error) => {
        throw new Error(`TelegramConstructor create error: ${error.message}`);
      });
  }

  public addEventListener(eventName: EventName, callback: EventListener): () => void;
  public addEventListener(eventErrorName: EventErrorName, callback: EventErrorListener): () => void;
  public addEventListener(eventName: EventName | EventErrorName, callback: EventListener | EventErrorListener) {
    const { listenersMap } = this;
    const events = listenersMap.get(eventName) ?? [];

    events.push(callback);
    listenersMap.set(eventName, events);

    return () => {
      const events = listenersMap.get(eventName) ?? [];
      const index = events.indexOf(callback);
      if (index !== -1) {
        events.splice(index, 1);
      }
    };
  }

  public addMessageCommand(command: MessageCommand<Context>) {
    const { messageController } = this;
    if (messageController == null) {
      throw new Error(
        'addMessageCommands error: messageController is not loaded yet, probably it is the problem in module',
      );
    }

    return messageController.addCommand(command);
  }

  public addMessageCommands(messageCommands: MessageCommand<Context>[]) {
    const commands: (() => void)[] = [];
    for (const messageCommand of messageCommands) {
      commands.push(this.addMessageCommand(messageCommand));
    }

    return commands;
  }

  public async input(sendOptions: SendOptions<Context>, inputOptions: InputOptions<Context>) {
    const chatId = sendOptions.chat_id;
    if (chatId == null) {
      throw new Error('input error: chat_id should be specified');
    }

    const chat = await this.chatController.getChat(chatId);
    if (chat.type !== 'private') {
      throw new Error('input error: input could not be used not in private chat');
    }

    this.inputController.addInputCommand({
      callback: inputOptions.callback,
      chat_id: sendOptions.chat_id,
    });

    return this.send(sendOptions);
  }

  public async cancelInput(chatId: number) {
    return this.inputController.cancelInput(chatId);
  }

  public async edit(editOptions: EditOptions<Context>) {
    const replyMarkup = this.getUpdatedReplyMarkup(editOptions, true);
    const options = {
      ...editOptions,
      reply_markup: replyMarkup,
    };

    const nerd = <T extends unknown[], R>(
      callback: Parameters<NerdFunction<T, R>>[1],
      ...args: Parameters<ReturnType<NerdFunction<T, R>>>
    ): R | Promise<R> => nerdFunction(options, callback.bind(this))(...args);

    let result: boolean | TelegramBot.Message | undefined;
    if ('text' in options) {
      result = await nerd(this.editMessageText, options.text, options);
    } else if (['photo', 'video', 'animation', 'audio', 'voice'].some((key) => key in options)) {
      result = await nerd(this.editMessageMedia, options, options);
    }
  }

  public async send(sendOptions: SendOptions<Context>) {
    const replyMarkup = this.getUpdatedReplyMarkup(sendOptions);
    const options = {
      ...sendOptions,
      reply_markup: replyMarkup,
    };

    const nerd = <T extends keyof TelegramBot, R>(methodName: T, ...args: Parameters<TelegramBot[T]>): R | Promise<R> =>
      nerdFunction(options, this.bindSuper(methodName))(...args);

    let result: TelegramBot.Message | undefined;
    if ('text' in options) {
      result = await nerd('sendMessage', options.chat_id, options.text, options);
    } else if ('photo' in options) {
      result = await nerd('sendPhoto', options.chat_id, options.photo, options, options);
    } else if ('media' in options) {
      result = await nerd('sendMediaGroup', options.chat_id, options.media, options);
    } else if ('video' in options) {
      result = await nerd('sendVideo', options.chat_id, options.video, options, options);
    } else if ('video_note' in options) {
      result = await nerd('sendVideoNote', options.chat_id, options.video_note, options, options);
    } else if ('animation' in options) {
      result = await nerd('sendAnimation', options.chat_id, options.animation, options);
    } else if ('audio' in options) {
      result = await nerd('sendAudio', options.chat_id, options.audio, options, options);
    } else if ('voice' in options) {
      result = await nerd('sendVoice', options.chat_id, options.voice, options, options);
    } else if ('question' in options) {
      result = await nerd('sendPoll', options.chat_id, options.question, options.poll_options, options);
    } else if ('sticker' in options) {
      result = await nerd('sendSticker', options.chat_id, options.sticker, options, options);
    } else if ('document' in options) {
      result = await nerd('sendDocument', options.chat_id, options.document, options, options);
    } else if ('emoji' in options) {
      result = await nerd('sendDice', options.chat_id, options);
    } else if ('game_short_name' in options) {
      result = await nerd('sendGame', options.chat_id, options.game_short_name, options);
    } else if ('provider_token' in options) {
      result = await nerd(
        'sendInvoice',
        options.chat_id,
        options.title,
        options.description,
        options.payload,
        options.provider_token,
        options.currency,
        options.prices,
        options,
      );
    } else if ('address' in options) {
      result = await nerd(
        'sendVenue',
        options.chat_id,
        options.latitude,
        options.longitude,
        options.title,
        options.address,
        options,
      );
    } else if ('latitude' in options) {
      result = await nerd('sendLocation', options.chat_id, options.latitude, options.longitude, options);
    } else if ('phone_number' in options) {
      result = await nerd('sendContact', options.chat_id, options.phone_number, options.first_name, options);
    }

    if (result == null) {
      throw new Error(`send error: invalid options sent\n${stringify(options)}`);
    }

    return new MessageEntity(this.getMethods(), result);
  }

  public async forward(options: ForwardOptions) {
    const result = await nerdFunction(options, this.bindSuper('forwardMessage'))(
      options.chat_id,
      options.from_chat_id,
      options.message_id,
      options,
    );

    return new MessageEntity(this.getMethods(), result);
  }

  public async copy(options: CopyOptions<Context>) {
    const replyMarkup = this.getUpdatedReplyMarkup(options);

    return nerdFunction(options, this.bindSuper('copyMessage'))(
      options.chat_id,
      options.from_chat_id,
      options.message_id,
      {
        ...options,
        reply_markup: replyMarkup,
      },
    );
  }

  private getUpdatedReplyMarkup(
    options: SendOptions<Context> | CopyOptions<Context>,
    onlyInline: true,
  ): TelegramBot.InlineKeyboardMarkup;
  private getUpdatedReplyMarkup(
    options: SendOptions<Context> | CopyOptions<Context>,
    onlyInline?: false,
  ):
    | TelegramBot.InlineKeyboardMarkup
    | TelegramBot.ReplyKeyboardMarkup
    | TelegramBot.ReplyKeyboardRemove
    | TelegramBot.ForceReply
    | undefined;
  private getUpdatedReplyMarkup(
    options: SendOptions<Context> | CopyOptions<Context>,
    onlyInline?: boolean,
  ):
    | TelegramBot.InlineKeyboardMarkup
    | TelegramBot.ReplyKeyboardMarkup
    | TelegramBot.ReplyKeyboardRemove
    | TelegramBot.ForceReply
    | undefined {
    const { callbackController, properties: constructorOptions } = this;
    const maxButtonsInRow = constructorOptions?.max_buttons_in_row ?? DEFAULT_MAX_BUTTONS_IN_ROW;

    if ('reply_markup' in options && options.reply_markup != null) {
      if ('inline_keyboard' in options.reply_markup && options.reply_markup.inline_keyboard != null) {
        return {
          inline_keyboard: callbackController.prepareInlineKeyboard(
            options.reply_markup.inline_keyboard,
            maxButtonsInRow,
          ),
        };
      }

      return onlyInline
        ? undefined
        : (options.reply_markup as
            | TelegramBot.ReplyKeyboardMarkup
            | TelegramBot.ReplyKeyboardRemove
            | TelegramBot.ForceReply);
    }
  }

  private addTelegramListenersOnCommands(): void {
    this.on('polling_error', this.onError.bind(this, 'polling_error'));
    this.on('webhook_error', this.onError.bind(this, 'webhook_error'));
    this.on('error', this.onError.bind(this, 'error'));

    this.on('message', (message) =>
      this.messageController.onMessage(this.getTelegramContext('message', message), message),
    );
    this.on('callback_query', (query) =>
      this.callbackQueryController.onCallback(this.getTelegramContext('callback_query', query), query),
    );
    // bot.onText(/\/start(?: (.+))?/, bindTelegramBot(onTelegramStartText));
    // telegramBot.on('inline_query', bindTelegramBot(onTelegramInlineQuery));
  }

  private onError(errorName: EventErrorName, err: Error) {
    const message = err.message;

    this.triggerEventListener(errorName, err);

    return message;
  }

  private getTelegramContext(eventName: 'message', data: TelegramBot.Message): Context;
  private getTelegramContext(eventName: 'callback_query', data: TelegramBot.CallbackQuery): Context;
  private getTelegramContext(eventName: TelegramDataEvents, data: TelegramData): Context {
    if (this.botUsername == null) {
      throw new Error('getTelegramContext error: botUsername is not loaded yet, probably it is the problem in module');
    }

    const telegramContext = {
      botUsername: this.botUsername,
      botToken: this.botToken,
    };

    // @ts-ignore
    return (this.options?.middlewares ?? []).reduce((acc: Partial<Context>, callback: Middlewares[number]) => {
      const result = callback({ ...telegramContext }, data);
      return Object.assign(acc, result);
    }, telegramContext);
  }

  private triggerEventListener(eventName: EventName): void;
  private triggerEventListener(eventErrorName: EventErrorName, err: Error): void;
  private triggerEventListener(eventName: EventName | EventErrorName, err?: Error) {
    const events = this.listenersMap.get(eventName) ?? [];

    if (err != null) {
      events.forEach((callback) => callback(err));
      return;
    }

    events.forEach((callback) => (callback as EventListener)());
  }

  private getCallbackByHash(hash: string) {
    return this.callbackController.getCallbackByHash(hash);
  }

  private getMethods(): Methods<Context> {
    return {
      send: this.send.bind(this),
      forward: this.forward.bind(this),
      copy: this.copy.bind(this),
      input: this.input.bind(this),
      getCallbackByHash: this.getCallbackByHash.bind(this),
    };
  }

  private bindSuper<T extends keyof TelegramBot>(methodName: T) {
    return (...args: Parameters<TelegramBot[T]>): ReturnType<TelegramBot[T]> => {
      if (typeof super[methodName] !== 'function') {
        throw new Error(`bindSuper error: ${methodName} is not a function`);
      }
      // @ts-ignore
      return this[methodName](...args);
    };
  }
}
