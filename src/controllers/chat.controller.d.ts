import TelegramBot from 'node-telegram-bot-api';
export declare class ChatController {
    private readonly bot;
    private chatIdToChatMap;
    constructor(bot: TelegramBot);
    getChat(chatId: TelegramBot.ChatId): Promise<TelegramBot.Chat>;
    private findAndAddChat;
}
