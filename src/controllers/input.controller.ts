import { TelegramContext } from '../types';
import { InputCommand } from '../types/inputCommand.type';
import { Methods } from '../types/constructor.type';
import TelegramBot from 'node-telegram-bot-api';

export class InputController<Context extends TelegramContext> {
  private chatIdToInputOptionsMap = new Map<TelegramBot.ChatId, InputCommand<Context>>();

  constructor(private readonly methods: Methods<Context>) {}

  public addInputCommand(inputCommand: InputCommand<Context>) {
    this.chatIdToInputOptionsMap.set(inputCommand.chat_id, inputCommand);

    return () => {
      this.chatIdToInputOptionsMap.delete(inputCommand.chat_id);
    };
  }

  public cancelInput(chatId: TelegramBot.ChatId) {
    this.chatIdToInputOptionsMap.delete(chatId);
  }

  public getInputCommand(chatId: TelegramBot.ChatId): InputCommand<Context> | undefined {
    const { chatIdToInputOptionsMap } = this;

    return chatIdToInputOptionsMap.get(chatId);
  }
}
