import { EventName, EventListener, TelegramContext, TelegramData, EventErrorName, EventErrorListener, SendOptions, ForwardOptions, CopyOptions, MessageCommand, EditOptions } from '../types';
import { MergeReturnType } from '../types/helper.type';
import { MessageEntity } from '../entities/message.entity';
import { InputOptions } from '../types/inputOptions.type';
export declare class ConstructorController<Initial extends TelegramContext, Middlewares extends ((arg: TelegramContext, data: TelegramData) => Initial)[], Context extends MergeReturnType<Initial, Middlewares> & TelegramContext> {
    private readonly botToken;
    private readonly options?;
    botUsername: string | undefined;
    private readonly listeners;
    private readonly bot;
    private readonly chatController;
    private readonly callbackController;
    private readonly callbackQueryController;
    private readonly inputController;
    private readonly messageController;
    constructor(botToken: string, options?: {
        middlewares?: Middlewares;
        attempts_count?: number;
        attempts_retry_timeout?: number;
        max_buttons_in_row?: number;
    });
    on(eventName: EventName, callback: EventListener): () => void;
    on(eventErrorName: EventErrorName, callback: EventErrorListener): () => void;
    addMessageCommand(command: MessageCommand<Context>): () => void;
    addMessageCommands(messageCommands: MessageCommand<Context>[]): (() => void)[];
    input(sendOptions: SendOptions<Context>, inputOptions: InputOptions<Context>): Promise<MessageEntity<Context>>;
    cancelInput(chatId: number): Promise<void>;
    edit(editOptions: EditOptions<Context>): Promise<void>;
    send(sendOptions: SendOptions<Context>): Promise<MessageEntity<Context>>;
    forward(options: ForwardOptions): Promise<MessageEntity<Context>>;
    copy(options: CopyOptions<Context>): Promise<unknown>;
    private getUpdatedReplyMarkup;
    private addTelegramListenersOnCommands;
    private onError;
    private getTelegramContext;
    private triggerEventListener;
    private getCallbackByHash;
    private getMethods;
}
