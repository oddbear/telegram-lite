"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallbackQueryController = void 0;
var callbackQuery_entity_1 = require("../entities/callbackQuery.entity");
var CallbackQueryController = /** @class */ (function () {
    function CallbackQueryController(methods, callbackController) {
        this.methods = methods;
        this.callbackController = callbackController;
    }
    CallbackQueryController.prototype.onCallback = function (context, query) {
        var data = query.data;
        if (data == null) {
            return;
        }
        var callback = this.callbackController.getCallbackByHash(data);
        if (callback == null) {
            throw new Error('onCallback error: undefined callbackQuery is used, probably it is the old callbackQuery instance, new inline_buttons should work correctly or you used own callback_data');
        }
        callback(context, new callbackQuery_entity_1.CallbackQueryEntity(this.methods, query));
    };
    return CallbackQueryController;
}());
exports.CallbackQueryController = CallbackQueryController;
