import { MessageCommand } from '../types/messageCommand.type';
import TelegramBot from 'node-telegram-bot-api';
import { TelegramContext } from '../types';
import { Methods } from '../types/constructor.type';
import { InputController } from './input.controller';
export declare class MessageController<Context extends TelegramContext> {
    private readonly methods;
    private readonly inputController;
    private readonly commands;
    constructor(methods: Methods<Context>, inputController: InputController<Context>);
    addCommand(command: MessageCommand<Context>): () => void;
    onMessage(context: Context, message: TelegramBot.Message): Promise<void>;
}
