"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallbackService = void 0;
var CallbackService = /** @class */ (function () {
    function CallbackService() {
        this.hashToCallbackMap = new Map();
    }
    CallbackService.prototype.prepareInlineKeyboard = function (inlineButtons, maxButtonsInRow) {
        var telegramButtons = [];
        for (var i = 0; i < inlineButtons.length; i++) {
            var button = inlineButtons[i];
            var text = button.text, row = button.row, callback = button.callback;
            var hash = this.getHash();
            this.hashToCallbackMap.set(hash, callback);
            var element = { text: text, callback_data: hash };
            if (row == null) {
                var currentRow = telegramButtons[telegramButtons.length - 1];
                if (currentRow == null) {
                    telegramButtons.push([element]);
                }
                else {
                    if (currentRow.length >= maxButtonsInRow) {
                        telegramButtons.push([element]);
                    }
                    else {
                        currentRow.push(element);
                    }
                }
            }
            else {
                if (telegramButtons[row] == null) {
                    telegramButtons[row] = [];
                }
                telegramButtons[row].push(element);
            }
        }
        return Object.values(telegramButtons.filter(function (row) { return row.length > 0; }));
    };
    CallbackService.prototype.getCallbackByHash = function (hash) {
        return this.hashToCallbackMap.get(hash);
    };
    CallbackService.prototype.getHash = function () {
        var date = new Date();
        var dateFormatted = "".concat(date.getUTCMonth()).concat(date.getUTCDate()).concat(date.getUTCFullYear());
        return "".concat(Math.random().toString(36).substring(2, 15)).concat(Math.random()
            .toString(36)
            .substring(2, 15)).concat(dateFormatted);
    };
    return CallbackService;
}());
exports.CallbackService = CallbackService;
