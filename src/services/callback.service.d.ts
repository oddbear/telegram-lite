import { TelegramContext } from '../types';
import { Callback, InlineKeyboardButton } from '../types/callbackQuery.type';
export declare class CallbackService<Context extends TelegramContext> {
    private readonly hashToCallbackMap;
    prepareInlineKeyboard(inlineButtons: InlineKeyboardButton<Context>[], maxButtonsInRow: number): TelegramBot.InlineKeyboardButton[][];
    getCallbackByHash(hash: string): Callback<Context>;
    private getHash;
}
