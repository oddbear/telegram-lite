import { TelegramContext } from '../types';
import TelegramBot from 'node-telegram-bot-api';
import { Callback, InlineKeyboardButton } from '../types/callbackQuery.type';

export class CallbackService<Context extends TelegramContext> {
  private readonly hashToCallbackMap = new Map<string, Callback<Context>>();

  public prepareInlineKeyboard(inlineButtons: InlineKeyboardButton<Context>[], maxButtonsInRow: number) {
    const telegramButtons: TelegramBot.InlineKeyboardButton[][] = [];

    for (let i = 0; i < inlineButtons.length; i++) {
      const button = inlineButtons[i];

      const { text, row, callback } = button;

      const hash = this.getHash();
      this.hashToCallbackMap.set(hash, callback);

      const element = { text, callback_data: hash };

      if (row == null) {
        const currentRow = telegramButtons[telegramButtons.length - 1];

        if (currentRow == null) {
          telegramButtons.push([element]);
        } else {
          if (currentRow.length >= maxButtonsInRow) {
            telegramButtons.push([element]);
          } else {
            currentRow.push(element);
          }
        }
      } else {
        if (telegramButtons[row] == null) {
          telegramButtons[row] = [];
        }

        telegramButtons[row].push(element);
      }
    }

    return Object.values(telegramButtons.filter((row) => row.length > 0));
  }

  public getCallbackByHash(hash: string) {
    return this.hashToCallbackMap.get(hash);
  }

  private getHash() {
    const date = new Date();
    const dateFormatted = `${date.getUTCMonth()}${date.getUTCDate()}${date.getUTCFullYear()}`;

    return `${Math.random().toString(36).substring(2, 15)}${Math.random()
      .toString(36)
      .substring(2, 15)}${dateFormatted}`;
  }
}
