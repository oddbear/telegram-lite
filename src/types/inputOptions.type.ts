import { Message } from './message.type';
import { TelegramContext } from './telegram.type';
import TelegramBot from 'node-telegram-bot-api';

export interface InputOptions<Context extends TelegramContext> {
  callback: InputCallback<Context>;
}

export type InputCallback<Context extends TelegramContext> = (
  context: Context,
  message: Message<Context>,
) => Promise<boolean> | boolean;
