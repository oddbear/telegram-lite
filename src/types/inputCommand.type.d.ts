import { TelegramContext } from './telegram.type';
import TelegramBot from 'node-telegram-bot-api';
import { InputCallback } from './inputOptions.type';
export interface InputCommand<Context extends TelegramContext> {
    callback: InputCallback<Context>;
    chat_id: TelegramBot.ChatId;
}
