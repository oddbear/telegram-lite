export const eventsNames = ['ready'] as const;
export type EventName = (typeof eventsNames)[number];

export const eventErrorsNames = ['polling_error', 'webhook_error', 'error'] as const;
export type EventErrorName = (typeof eventErrorsNames)[number];

export type EventListener = () => void;
export type EventErrorListener = (err: Error) => void;
