"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.eventErrorsNames = exports.eventsNames = void 0;
exports.eventsNames = ['ready'];
exports.eventErrorsNames = ['polling_error', 'webhook_error', 'error'];
