export type DeepPartial<T> = {
  [P in keyof T]?: T[P] extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T[P] extends ReadonlyArray<infer U>
    ? ReadonlyArray<DeepPartial<U>>
    : DeepPartial<T[P]>;
};

type FuncUnionToIntersection<U> = (U extends any ? (k: U) => void : never) extends (k: infer I) => void ? I : never;
export type MergeReturnType<T, V> = V extends ((arg: T) => infer D)[] ? FuncUnionToIntersection<D> : never;

export type NoMarkup<T> = Omit<T, 'reply_markup'>;
