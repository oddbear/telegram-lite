import TelegramBot from 'node-telegram-bot-api';
import { Stream } from 'stream';
import { BasicOptions } from './basicOptions.type';
import { InlineKeyboardButton } from './callbackQuery.type';
import { TelegramContext } from './telegram.type';
import { NoMarkup } from './helper.type';

export type SendOptions<
  Context extends TelegramContext,
  Unsigned extends true | false = false,
> = BasicOptions<Unsigned> & {
  reply_markup?:
    | {
        inline_keyboard?: InlineKeyboardButton<Context>[];
      }
    | TelegramBot.ReplyKeyboardMarkup
    | TelegramBot.ReplyKeyboardRemove
    | TelegramBot.ForceReply;
} & (
    | ({ text: string } & NoMarkup<TelegramBot.SendMessageOptions>)
    | ({
        photo: string | Stream | Buffer;
      } & NoMarkup<TelegramBot.SendPhotoOptions> &
        TelegramBot.FileOptions)
    | ({
        media: ReadonlyArray<TelegramBot.InputMedia>;
      } & NoMarkup<TelegramBot.SendMediaGroupOptions>)
    | ({
        video: string | Stream | Buffer;
      } & NoMarkup<TelegramBot.SendVideoOptions> &
        TelegramBot.FileOptions)
    | ({
        video_note: string | Stream | Buffer;
      } & NoMarkup<TelegramBot.SendVideoNoteOptions> &
        TelegramBot.FileOptions)
    | ({
        animation: string | Stream | Buffer;
      } & NoMarkup<TelegramBot.SendAnimationOptions> &
        TelegramBot.FileOptions)
    | ({
        audio: string | Stream | Buffer;
      } & NoMarkup<TelegramBot.SendAudioOptions> &
        TelegramBot.FileOptions)
    | ({
        voice: string | Stream | Buffer;
      } & NoMarkup<TelegramBot.SendVoiceOptions> &
        TelegramBot.FileOptions)
    | ({
        question: string;
        poll_options: ReadonlyArray<string>;
      } & NoMarkup<TelegramBot.SendPollOptions>)
    | ({
        sticker: string | Stream | Buffer;
      } & NoMarkup<TelegramBot.SendStickerOptions> &
        TelegramBot.FileOptions)
    | NoMarkup<TelegramBot.SendDiceOptions>
    | ({
        document: string | Stream | Buffer;
      } & NoMarkup<TelegramBot.SendDocumentOptions> &
        TelegramBot.FileOptions)
    | ({
        game_short_name: string;
      } & NoMarkup<TelegramBot.SendGameOptions>)
    | ({
        title: string;
        description: string;
        payload: string;
        provider_token: string;
        currency: string;
        prices: ReadonlyArray<TelegramBot.LabeledPrice>;
      } & NoMarkup<TelegramBot.SendInvoiceOptions>)
    | ({
        latitude: number;
        longitude: number;
        title: string;
        address: string;
      } & NoMarkup<TelegramBot.SendVenueOptions>)
    | ({
        latitude: number;
        longitude: number;
      } & NoMarkup<TelegramBot.SendLocationOptions>)
    | ({
        phone_number: string;
        first_name: string;
      } & NoMarkup<TelegramBot.SendContactOptions>)
    | ({
        action: TelegramBot.ChatAction;
      } & NoMarkup<TelegramBot.SendChatActionOptions>)
  );
