import { TelegramContext } from './telegram.type';
import { Message } from './message.type';
export type MessageCommand<T extends TelegramContext> = {
    name?: string;
    callback: (telegramContext: T, message: Message<T>) => void;
} & ({
    validation: (telegramContext: T, message: Message<T>) => boolean;
} | {
    regexp: RegExp;
} | {
    text: string;
});
