import { SendOptions } from './sendOptions.type';
import { MessageEntity } from '../entities/message.entity';
import { TelegramContext } from './telegram.type';
import { ForwardOptions } from './forwardOptions.type';
import { CopyOptions } from './copyOptions.type';
import { Callback } from './callbackQuery.type';
import TelegramBot from 'node-telegram-bot-api';
import { InputOptions } from './inputOptions.type';

export interface Methods<Context extends TelegramContext> {
  send: (options: SendOptions<Context>) => Promise<MessageEntity<Context>>;
  forward: (options: ForwardOptions) => Promise<MessageEntity<Context>>;
  copy: (options: CopyOptions<Context>) => Promise<TelegramBot.MessageId>;
  input: (options: SendOptions<Context>, inputOptions: InputOptions<Context>) => Promise<MessageEntity<Context>>;
  getCallbackByHash: (hash: string) => Callback<Context> | undefined;
}
