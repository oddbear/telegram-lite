import TelegramBot from 'node-telegram-bot-api';
import { BasicOptions } from './basicOptions.type';
import { TelegramContext } from './telegram.type';
import { InlineKeyboardButton } from './callbackQuery.type';
import { NoMarkup } from './helper.type';
export type CopyOptions<Context extends TelegramContext, Unsigned extends false | true = false> = BasicOptions<Unsigned> & {
    reply_markup?: {
        inline_keyboard?: InlineKeyboardButton<Context>[];
    } | TelegramBot.ReplyKeyboardMarkup | TelegramBot.ReplyKeyboardRemove | TelegramBot.ForceReply;
    from_chat_id: TelegramBot.ChatId;
    message_id: number;
} & NoMarkup<TelegramBot.CopyMessageOptions>;
