import TelegramBot from 'node-telegram-bot-api';
import { MessageEntity } from '../entities/message.entity';
import { TelegramContext } from './telegram.type';
import { InlineKeyboardButton } from './callbackQuery.type';
export type Message<Context extends TelegramContext> = Omit<TelegramBot.Message, 'reply_markup'> & MessageEntity<Context> & {
    reply_markup?: {
        inline_keyboard?: InlineKeyboardButton<Context>[];
    };
    reply_to_message?: Message<Context>;
};
export type UnsignedMessage = Omit<TelegramBot.Message, 'reply_markup'> & {
    reply_markup?: {
        inline_keyboard?: any;
    };
    reply_to_message?: UnsignedMessage;
};
