import TelegramBot from 'node-telegram-bot-api';

export interface TelegramContext {
  botUsername: string;
  botToken: string;
}

export const telegramDataEventsNames = ['message', 'callback_query', 'inline_query'] as const;
export type TelegramDataEvents = (typeof telegramDataEventsNames)[number];

export type TelegramData = TelegramBot.Message | TelegramBot.CallbackQuery | TelegramBot.InlineQuery;
