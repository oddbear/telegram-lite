import TelegramBot from 'node-telegram-bot-api';
export interface TelegramContext {
    bot: TelegramBot;
    botUsername: string;
    botToken: string;
}
export declare const telegramDataEventsNames: readonly ["message", "callback_query", "inline_query"];
export type TelegramDataEvents = (typeof telegramDataEventsNames)[number];
export type TelegramData = TelegramBot.Message | TelegramBot.CallbackQuery | TelegramBot.InlineQuery;
