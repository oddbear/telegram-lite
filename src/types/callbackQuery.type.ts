import TelegramBot from 'node-telegram-bot-api';
import { TelegramContext } from './telegram.type';
import { CallbackQueryEntity } from '../entities/callbackQuery.entity';
import { Message } from './message.type';

export type Callback<Context extends TelegramContext> = (context: Context, query: CallbackQueryEntity<Context>) => void;

export interface InlineKeyboardButton<Context extends TelegramContext>
  extends Omit<TelegramBot.InlineKeyboardButton, 'callback_data'> {
  text: string;
  callback: Callback<Context>;
  row?: number;
}

export type CallbackQuery<Context extends TelegramContext> = Omit<TelegramBot.CallbackQuery, 'message'> &
  CallbackQueryEntity<Context> & {
    message?: Message<Context>;
  };
