import { Message } from './message.type';
import { TelegramContext } from './telegram.type';
export interface InputOptions<Context extends TelegramContext> {
    callback: InputCallback<Context>;
}
export type InputCallback<Context extends TelegramContext> = (context: Context, message: Message<Context>) => Promise<boolean> | boolean;
