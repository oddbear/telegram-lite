import TelegramBot from 'node-telegram-bot-api';
import { Stream } from 'stream';
import { BasicOptions } from './basicOptions.type';
import { InlineKeyboardButton } from './callbackQuery.type';
import { TelegramContext } from './telegram.type';
import { NoMarkup } from './helper.type';

export type EditOptions<
  Context extends TelegramContext,
  Unsigned extends true | false = false,
> = BasicOptions<Unsigned> & {
  reply_markup?: {
    inline_keyboard?: InlineKeyboardButton<Context>[];
  };
} & (
    | ({ text: string } & NoMarkup<TelegramBot.EditMessageTextOptions>)
    | ({
        photo: string | Stream | Buffer;
      } & NoMarkup<TelegramBot.EditMessageMediaOptions> &
        TelegramBot.InputMediaPhoto)
    | ({
        video: string | Stream | Buffer;
      } & NoMarkup<TelegramBot.EditMessageMediaOptions> &
        TelegramBot.InputMediaVideo)
    | ({
        animation: string | Stream | Buffer;
      } & NoMarkup<TelegramBot.EditMessageMediaOptions> &
        TelegramBot.InputMedia)
    | ({
        audio: string | Stream | Buffer;
      } & NoMarkup<TelegramBot.EditMessageMediaOptions> &
        TelegramBot.InputMedia)
    | ({
        voice: string | Stream | Buffer;
      } & NoMarkup<TelegramBot.EditMessageMediaOptions> &
        TelegramBot.InputMedia)
  );
