export declare const eventsNames: readonly ["ready"];
export type EventName = (typeof eventsNames)[number];
export declare const eventErrorsNames: readonly ["polling_error", "webhook_error", "error"];
export type EventErrorName = (typeof eventErrorsNames)[number];
export type EventListener = () => void;
export type EventErrorListener = (err: Error) => void;
