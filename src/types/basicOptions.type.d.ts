import TelegramBot from 'node-telegram-bot-api';
export type BasicOptions<Unsigned = false> = Unsigned extends true ? {
    attempts_count?: number;
    attempts_retry_timeout?: number;
} : {
    attempts_count?: number;
    attempts_retry_timeout?: number;
    chat_id: TelegramBot.ChatId;
};
