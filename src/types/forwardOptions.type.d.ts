import TelegramBot from 'node-telegram-bot-api';
import { BasicOptions } from './basicOptions.type';
export type ForwardOptions = BasicOptions & {
    from_chat_id: TelegramBot.ChatId;
    message_id: number;
} & TelegramBot.ForwardMessageOptions;
