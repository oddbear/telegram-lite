"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageEntity = void 0;
var MessageEntity = /** @class */ (function () {
    function MessageEntity(methods, message) {
        this.methods = methods;
        this.message = this.updateMessage(message);
    }
    MessageEntity.prototype.answer = function (options) {
        return this.methods.send(__assign(__assign({}, options), { chat_id: this.message.chat.id }));
    };
    MessageEntity.prototype.reply = function (options) {
        return this.methods.send(__assign(__assign({}, options), { reply_to_message_id: this.message.message_id, chat_id: this.message.chat.id }));
    };
    MessageEntity.prototype.forward = function (options) {
        return this.methods.forward(__assign(__assign({}, options), { message_id: this.message.message_id, from_chat_id: this.message.chat.id }));
    };
    MessageEntity.prototype.copy = function (options) {
        return this.methods.copy(__assign(__assign({}, options), { message_id: this.message.message_id, from_chat_id: this.message.chat.id }));
    };
    MessageEntity.prototype.input = function (options, inputOptions) {
        return this.methods.input(__assign(__assign({}, options), { chat_id: this.message.chat.id }), inputOptions);
    };
    MessageEntity.prototype.updateMessage = function (message) {
        var _a;
        var methods = this.methods;
        var floatMessage = message;
        while (((_a = floatMessage === null || floatMessage === void 0 ? void 0 : floatMessage.reply_markup) === null || _a === void 0 ? void 0 : _a.inline_keyboard) != null) {
            var inlineKeyboard = [];
            for (var _i = 0, _b = floatMessage.reply_markup.inline_keyboard; _i < _b.length; _i++) {
                var row = _b[_i];
                for (var _c = 0, row_1 = row; _c < row_1.length; _c++) {
                    var button = row_1[_c];
                    if (button.callback_data != null) {
                        button.callback = methods.getCallbackByHash(button.callback_data);
                        button.callback_data = undefined;
                    }
                    inlineKeyboard.push(__assign(__assign({}, button), { row: row }));
                }
            }
            floatMessage.reply_markup.inline_keyboard = inlineKeyboard;
            floatMessage = message.reply_to_message;
        }
        return __assign(__assign({}, this.getMethods()), message);
    };
    MessageEntity.prototype.getMethods = function () {
        return {
            answer: this.answer.bind(this),
            reply: this.reply.bind(this),
            forward: this.forward.bind(this),
            copy: this.copy.bind(this),
            input: this.input.bind(this),
        };
    };
    return MessageEntity;
}());
exports.MessageEntity = MessageEntity;
