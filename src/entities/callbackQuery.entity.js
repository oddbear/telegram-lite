"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CallbackQueryEntity = void 0;
var message_entity_1 = require("./message.entity");
var CallbackQueryEntity = /** @class */ (function () {
    function CallbackQueryEntity(methods, query) {
        this.methods = methods;
        this.callbackQuery = __assign(__assign(__assign({}, this), query), { message: query.message == null ? undefined : new message_entity_1.MessageEntity(methods, query.message).message });
    }
    CallbackQueryEntity.prototype.answer = function (options) {
        var _a = this, methods = _a.methods, callbackQuery = _a.callbackQuery;
        var message = callbackQuery.message;
        if (message == null) {
            throw new Error('CallbackQueryEntity answer error: message not found for callback query');
        }
        var chatId = message.chat.id;
        return methods.send(__assign(__assign({}, options), { chat_id: chatId }));
    };
    return CallbackQueryEntity;
}());
exports.CallbackQueryEntity = CallbackQueryEntity;
