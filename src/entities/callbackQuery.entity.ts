import { SendOptions, TelegramContext } from '../types';
import TelegramBot from 'node-telegram-bot-api';
import { Methods } from '../types/constructor.type';
import { CallbackQuery } from '../types/callbackQuery.type';
import { MessageEntity } from './message.entity';

export class CallbackQueryEntity<Context extends TelegramContext> {
  private readonly callbackQuery: CallbackQuery<Context>;

  constructor(private readonly methods: Methods<Context>, query: TelegramBot.CallbackQuery) {
    this.callbackQuery = {
      ...this,
      ...query,
      message: query.message == null ? undefined : new MessageEntity<Context>(methods, query.message).message,
    };
  }

  public answer(options: SendOptions<Context, true>) {
    const { methods, callbackQuery } = this;

    const { message } = callbackQuery;
    if (message == null) {
      throw new Error('CallbackQueryEntity answer error: message not found for callback query');
    }
    const {
      chat: { id: chatId },
    } = message;

    return methods.send({
      ...options,
      chat_id: chatId,
    });
  }
}
