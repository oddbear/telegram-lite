import { CopyOptions, ForwardOptions, SendOptions, TelegramContext } from '../types';
import TelegramBot from 'node-telegram-bot-api';
import { Methods } from '../types/constructor.type';
import { Message } from '../types/message.type';
import { InputOptions } from '../types/inputOptions.type';
export declare class MessageEntity<Context extends TelegramContext> {
    private readonly methods;
    readonly message: Message<Context>;
    constructor(methods: Methods<Context>, message: TelegramBot.Message);
    answer(options: SendOptions<Context, true>): Promise<MessageEntity<Context>>;
    reply(options: SendOptions<Context, true>): Promise<MessageEntity<Context>>;
    forward(options: Omit<ForwardOptions, 'message_id' | 'from_chat_id'>): Promise<MessageEntity<Context>>;
    copy(options: Omit<CopyOptions<Context>, 'message_id' | 'from_chat_id'>): Promise<TelegramBot.MessageId>;
    input(options: SendOptions<Context, true>, inputOptions: InputOptions<Context>): Promise<MessageEntity<Context>>;
    private updateMessage;
    private getMethods;
}
