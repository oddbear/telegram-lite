import { SendOptions, TelegramContext } from '../types';
import TelegramBot from 'node-telegram-bot-api';
import { Methods } from '../types/constructor.type';
import { MessageEntity } from './message.entity';
export declare class CallbackQueryEntity<Context extends TelegramContext> {
    private readonly methods;
    private readonly callbackQuery;
    constructor(methods: Methods<Context>, query: TelegramBot.CallbackQuery);
    answer(options: SendOptions<Context, true>): Promise<MessageEntity<Context>>;
}
