import { CopyOptions, ForwardOptions, SendOptions, TelegramContext } from '../types';
import TelegramBot from 'node-telegram-bot-api';
import { Methods } from '../types/constructor.type';
import { InlineKeyboardButton } from '../types/callbackQuery.type';
import { Message, UnsignedMessage } from '../types/message.type';
import { InputOptions } from '../types/inputOptions.type';

export class MessageEntity<Context extends TelegramContext> {
  public readonly message: Message<Context>;

  constructor(private readonly methods: Methods<Context>, message: TelegramBot.Message) {
    this.message = this.updateMessage(message);
  }

  public answer(options: SendOptions<Context, true>) {
    return this.methods.send({
      ...options,
      chat_id: this.message.chat.id,
    });
  }

  public reply(options: SendOptions<Context, true>) {
    return this.methods.send({
      ...options,
      reply_to_message_id: this.message.message_id,
      chat_id: this.message.chat.id,
    });
  }

  public forward(options: Omit<ForwardOptions, 'message_id' | 'from_chat_id'>) {
    return this.methods.forward({
      ...options,
      message_id: this.message.message_id,
      from_chat_id: this.message.chat.id,
    });
  }

  public copy(options: Omit<CopyOptions<Context>, 'message_id' | 'from_chat_id'>) {
    return this.methods.copy({
      ...options,
      message_id: this.message.message_id,
      from_chat_id: this.message.chat.id,
    });
  }

  public input(options: SendOptions<Context, true>, inputOptions: InputOptions<Context>) {
    return this.methods.input(
      {
        ...options,
        chat_id: this.message.chat.id,
      },
      inputOptions,
    );
  }

  private updateMessage(message: TelegramBot.Message) {
    const { methods } = this;

    let floatMessage: UnsignedMessage | undefined = message;
    while (floatMessage?.reply_markup?.inline_keyboard != null) {
      const inlineKeyboard: InlineKeyboardButton<Context>[] = [];

      for (const row of floatMessage.reply_markup.inline_keyboard) {
        for (const button of row) {
          if (button.callback_data != null) {
            button.callback = methods.getCallbackByHash(button.callback_data);
            button.callback_data = undefined;
          }

          inlineKeyboard.push({
            ...button,
            row,
          });
        }
      }

      (floatMessage as Message<Context>).reply_markup!.inline_keyboard = inlineKeyboard;
      floatMessage = message.reply_to_message;
    }

    return {
      ...this.getMethods(),
      ...message,
    } as Message<Context>;
  }

  private getMethods() {
    return {
      answer: this.answer.bind(this),
      reply: this.reply.bind(this),
      forward: this.forward.bind(this),
      copy: this.copy.bind(this),
      input: this.input.bind(this),
    };
  }
}
