const defaultAttemptsCount = 1;
const defaultAttemptsRetryTimeout = 3000;

export type NerdFunction<P extends unknown[], R> = (
  attemptsCount: number,
  callback: (...args: P) => Promise<R> | R,
) => (...args: P) => Promise<R>;

export const nerdFunction =
  <P extends unknown[], R>(
    {
      attempts_count = defaultAttemptsCount,
      attempts_retry_timeout = defaultAttemptsRetryTimeout,
    }: {
      attempts_count?: number;
      attempts_retry_timeout?: number;
    },
    callback: (...args: P) => Promise<R> | R,
  ) =>
  (...args: P): Promise<R> => {
    let tries = 0;

    return new Promise((res, rej) => {
      void (async function init() {
        try {
          res(await callback(...args));
        } catch (err) {
          if (attempts_count <= ++tries) {
            rej(err);

            return;
          }

          setTimeout(() => {
            void init();
          }, attempts_retry_timeout);
        }
      })();
    });
  };
