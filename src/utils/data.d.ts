export type NerdFunction<P extends unknown[], R> = (attemptsCount: number, callback: (...args: P) => Promise<R> | R) => (...args: P) => Promise<R>;
export declare const nerdFunction: <P extends unknown[], R>({ attempts_count, attempts_retry_timeout, }: {
    attempts_count?: number;
    attempts_retry_timeout?: number;
}, callback: (...args: P) => R | Promise<R>) => (...args: P) => Promise<R>;
