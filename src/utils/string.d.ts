export declare const escapeMarkdown2Characters: (text: string) => string;
export declare const stringify: (data: unknown) => string;
