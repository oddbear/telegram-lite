"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stringify = exports.escapeMarkdown2Characters = void 0;
var escapeMarkdown2Characters = function (text) {
    return text.replace(/([_\-*[\]()~`>#+=|{}.!])/g, '\\$1');
};
exports.escapeMarkdown2Characters = escapeMarkdown2Characters;
var stringify = function (data) {
    return JSON.stringify(data, null, 2);
};
exports.stringify = stringify;
