export const escapeMarkdown2Characters = (text: string): string => {
  return text.replace(/([_\-*[\]()~`>#+=|{}.!])/g, '\\$1');
};

export const stringify = (data: unknown): string => {
  return JSON.stringify(data, null, 2);
};
