import axios from 'axios';

export const getTelegramFileUrl = (botToken: string, filePath: string): string => {
  return `https://api.telegram.org/file/bot${botToken}/${filePath}`;
};

export const getBotUsername = async (botToken: string): Promise<string> => {
  return axios
    .get<{ result: { username: string } }>(`https://api.telegram.org/bot${botToken}/getMe`)
    .then((response) => {
      return response.data.result.username;
    });
};
