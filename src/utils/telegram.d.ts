export declare const getTelegramFileUrl: (botToken: string, filePath: string) => string;
export declare const getBotUsername: (botToken: string) => Promise<string>;
